   package Apiscripts;

   import java.util.ArrayList;
   import org.apache.http.NameValuePair;
   import org.apache.http.client.ClientProtocolException;
   import com.Utility.ExecutorHelper;
   import com.Utility.UtilityHelper;

   public class P0_getapitoken extends ExecutorHelper {
   ExecutorHelper executorHelper;

   P0_getapitoken(String url) {
   super(url);
   }

   public static void main(String[] args) throws ClientProtocolException, Throwable {
   P0_getapitoken obj = new P0_getapitoken("https://stage.upwork.com");
   obj.getToken("/api/v3/oauth2/token/", UtilityHelper.loginheader(), new ArrayList<NameValuePair>());
   }

}
