package com.Utility;
import java.util.HashMap;
import java.util.Set;
import org.junit.Assert;
import com.Utility.ResponseHelper;

public class ValidatorHelper extends UtilityHelper {
	
public ResponseHelper response;	

protected ValidatorHelper(ResponseHelper response) {
this.response = response;
}

public ValidatorHelper expectCode(int expectedCode) {
Assert.assertEquals("Incorrect Response Code", expectedCode, response.getResponseCode());
System.out.println(response.getResponseCode());
return this;
}

public ValidatorHelper expectMessage(String message) {
Assert.assertEquals("Incorrect Response Message", message, response.getResponseMessage());
System.out.println(response.getResponseMessage());
return this;
}

/*public ValidatorHelper expectHeader(String headerName, String headerValue) {
Assert.assertEquals("Incorrect header - " + headerName, headerValue, response.getHeader(headerName));
return this;
}*/

/*public ValidatorHelper expectHeaders(HashMap<String, String> headers) {
Set<String> keys = headers.keySet();
for (String key : keys) {
Assert.assertEquals("Incorrect header - " + key, headers.get(key), response.getHeader(key));
}
return this;
}*/

public ValidatorHelper expectInBody(String content) {
Assert.assertTrue("Body doesnt contain string : " + content, response.getResponseBody().contains(content));
System.out.println(response.getResponseBody());
return this;
}

public ValidatorHelper printBody() {
System.out.println(response.getResponseBody());
return this;
}

public  ResponseHelper getResponse() {
return response;
}

}
