    package com.Utility;

    import java.io.FileInputStream;
     import java.io.FileOutputStream;
   import java.io.OutputStream;

   import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
    import org.apache.poi.hssf.usermodel.HSSFSheet;
    import org.apache.poi.hssf.usermodel.HSSFWorkbook;
    import org.apache.poi.ss.usermodel.Cell;
    import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.RichTextString;

    public class ExcelUtils {
	
    public static HSSFWorkbook workbook;
    public static HSSFSheet sheet;
    public static HSSFRow row;
    public static HSSFCell cell;
    final static String projectDir = System.getProperty("user.dir");
	public static final String reportpath = projectDir + "\\Reporting.xls";
	//public static final String reportpath = "C:/Users/360LP0089/Documents/Reporting.xls";
	
   	
   	int rowNum =1;

   	public static void setExcelFile ( ) throws Exception {
   		
   	try {
   	FileInputStream Fin = new FileInputStream(reportpath);
   	FileOutputStream Fout = new FileOutputStream(reportpath);
   	System.out.println(Fin.hashCode());
   	workbook = new HSSFWorkbook(Fin);
   	sheet = workbook.getSheetAt(0);
   	}
   		
   	catch ( Exception e){
   			
   	throw (e);
   	}
   	
   	}
   	
   		
   public static void getCellData ( int RowNum, int ColNum) throws Exception {
   		
   setExcelFile ( );
   	
   try {
   
   cell = sheet.getRow(RowNum).getCell(ColNum);
  
   switch (cell.getCellType()) {
   
   case Cell.CELL_TYPE_BOOLEAN:
   System.out.println(cell.getBooleanCellValue());
   break;
   case Cell.CELL_TYPE_NUMERIC:
   System.out.println(cell.getNumericCellValue());
   break;
   case Cell.CELL_TYPE_STRING:
   System.out.println(cell.getStringCellValue());
   break;
   case Cell.CELL_TYPE_BLANK:
   break;
   case Cell.CELL_TYPE_ERROR:
   System.out.println(cell.getErrorCellValue());
   break;

   }
   }
   	
   catch ( Exception e){
			
   throw (e);
	
   }

   }
    
   
   public static void updateExcelFile ( int RowNum, int ColNum) throws Exception {
	
   try {
		   
   FileInputStream Fin = new FileInputStream(reportpath);
   System.out.println(Fin.hashCode());
   workbook = new HSSFWorkbook(Fin);
   sheet = workbook.getSheetAt(0);
   HSSFRow row = sheet.getRow(RowNum);
   HSSFCell cell = row.getCell(ColNum);
   cell.setCellValue(200);
   System.out.println(cell);
   Fin.close();
   FileOutputStream Fout = new FileOutputStream(reportpath);
   workbook.write(Fout);
   Fout.close();
		
   }
	   	
   catch ( Exception e){
				
   throw (e);
		
   }
	
	
   }
   	
   	   	
  /* public static void main(String[] args) throws Throwable
   {
   	ExcelUtils onnn = new ExcelUtils();
   	ExcelUtils.updateExcelFile(1, 3);
   	
      	   		
   }*/

	
   }
    
